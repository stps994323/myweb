﻿using System;
using System.Collections.Generic;

namespace Web_Test.Models;

public partial class Teacher
{
    public string Id { get; set; } = null!;

    public string TeacherName { get; set; } = null!;
}

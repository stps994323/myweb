﻿using System;
using System.Collections.Generic;

namespace Web_Test.Models;

public partial class Image
{
    public int Id { get; set; }

    public string? Name { get; set; }

    public string? ImageData { get; set; }
}

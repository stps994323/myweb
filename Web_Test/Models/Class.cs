﻿using System;
using System.Collections.Generic;

namespace Web_Test.Models;

public partial class Class
{
    public string Id { get; set; } = null!;

    public string? Name { get; set; }

    public string TeacherId { get; set; } = null!;
}

﻿using System;
using System.Collections.Generic;

namespace Web_Test.Models;

public partial class Student
{
    public string Id { get; set; } = null!;

    public string Name { get; set; } = null!;

    public string? ClassId { get; set; }

    public string Status { get; set; } = null!;

    public DateTime CreatTime { get; set; }

    public DateTime? UpdateTime { get; set; }
}

﻿using System;
using Web_Test.Models.Entity;

namespace Web_Test.Models.Logic
{
    
    public class Messagelogic
    {

        public List<MessageBoard> GetMessage()
        {
            MessageContext messagecontext = new MessageContext();

            var list = messagecontext.MessageBoards.AsQueryable();

            foreach (var item in list)
            {

            }
            return list.ToList();
        }

        public void SaveMessage(string name, string message)
        {
            MessageContext messagecontext = new MessageContext();

            messagecontext.MessageBoards.Add(new MessageBoard
            {

                Name = name,
                Message = message,
                CreatTime = DateTime.Now,
                UpdateTime = DateTime.Now,
            });
            messagecontext.SaveChanges();
        }

        public void UpdateMessage(string name, string message, int sn)
        {
            MessageContext messagecontext = new MessageContext();

            var data = messagecontext.MessageBoards.FirstOrDefault(f => f.Sn == sn);
            if (data != null)
            {
                data.Name = name;
                data.Message = message;
                data.UpdateTime = DateTime.Now;
            }
            messagecontext.SaveChanges();
        }

        public void DeleteMessage(int sn) 
        {
            MessageContext messageContext = new MessageContext();
            var data = messageContext.MessageBoards.FirstOrDefault(f => f.Sn == sn);
            if(data != null)
            {
                messageContext.MessageBoards.Remove(data);
                messageContext.SaveChanges();
            }
        
        
        }

        

       
    }
}

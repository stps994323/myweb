﻿namespace Web_Test.Models.Logic
{
    public class Countlogic
    {
       public double Num1 { get; set; }
       public double Num2 { get; set; }
       public string ? Counts { get; set; }


      

       public double Result
        {
            get
            {
                switch (Counts)
                {
                    case "addition":
                        return(Num1 + Num2);
                        break;

                    case "subtraction":
                        return(Num1 - Num2);
                        break;

                    case "multiplication":
                        return(Num1 * Num2);
                        break;

                    case "division":
                       
                        return(Num1 / Num2);                                              
                        break;

                    default:
                        return double.NaN;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Web_Test.Models.Entity;

public partial class Anime
{
    public string Name { get; set; } = null!;

    public string? Code { get; set; }

    public DateTime CreatTime { get; set; }

    public DateTime? UpdateTime { get; set; }
}

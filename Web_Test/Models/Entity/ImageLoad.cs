﻿using System;
using System.Collections.Generic;

namespace Web_Test.Models.Entity;

public partial class ImageLoad
{
    public string Id { get; set; } = null!;

    public string? Name { get; set; }

    public string? ImageData { get; set; }

    public DateTime CreatTimes { get; set; }

    public DateTime? UpdateTimes { get; set; }
}

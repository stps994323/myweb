﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Web_Test.Models.Entity;

public partial class MessageContext : DbContext
{
    public MessageContext()
    {
    }

    public MessageContext(DbContextOptions<MessageContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Anime> Animes { get; set; }

    public virtual DbSet<MessageBoard> MessageBoards { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=localhost;Database=Message;User Id=teacher;Password=4323;TrustServerCertificate=true;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Anime>(entity =>
        {
            entity.HasKey(e => e.Name);

            entity.ToTable("Anime");

            entity.Property(e => e.Name).HasMaxLength(20);
            entity.Property(e => e.Code)
                .HasMaxLength(10)
                .IsUnicode(false);
            entity.Property(e => e.CreatTime)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.UpdateTime).HasColumnType("datetime");
        });

        modelBuilder.Entity<MessageBoard>(entity =>
        {
            entity.HasKey(e => e.Sn);

            entity.ToTable("Message_board");

            entity.Property(e => e.AnimeCode)
                .HasMaxLength(10)
                .IsUnicode(false)
                .HasColumnName("Anime_Code");
            entity.Property(e => e.CreatTime)
                .HasDefaultValueSql("(getdate())")
                .HasColumnType("datetime");
            entity.Property(e => e.Message).HasMaxLength(50);
            entity.Property(e => e.Name).HasMaxLength(10);
            entity.Property(e => e.UpdateTime).HasColumnType("datetime");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}

﻿using System;
using System.Collections.Generic;

namespace Web_Test.Models.Entity;

public partial class MessageBoard
{
    public int Sn { get; set; }

    public string? Name { get; set; }

    public string? Message { get; set; }

    public DateTime CreatTime { get; set; }

    public DateTime? UpdateTime { get; set; }

    public string? AnimeCode { get; set; }
}

﻿using Microsoft.AspNetCore.Mvc;
using Web_Test.Models.Logic;

namespace Web_Test.Controllers
{
    public class CountController:Controller
    {
        [HttpGet]
        public IActionResult Count()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(Countlogic model)
        {
            ViewBag.Result = "您的結果值";
            if (ModelState.IsValid)
            {

                double result = model.Result;

                ViewBag.Result = result;
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult GetResult(int num1 , int num2 , string counts)
        {
           var countlogic = new Countlogic();
            {
                countlogic.Num1 = num1;
                countlogic.Num2 = num2;
                countlogic.Counts = counts;
            };
           

            return Json(countlogic.Result);
        }
       
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Web_Test.Models.Logic;
using System.Web;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace Web_Test.Controllers
{
    
    public class ImageController : Controller
    {
        
        private List<Imagelogic> imageInfoList = new List<Imagelogic>();
        private readonly ILogger<ImageController> _logger;
        private readonly IWebHostEnvironment _env;

        public ImageController(ILogger<ImageController> logger, IWebHostEnvironment env)
        {
            _logger = logger;
            _env = env;
           
        }

        

        [HttpGet]
        public IActionResult ImageUpload()
        {

            return View();
        }

        [HttpPost]
        public IActionResult ImageUpload(int sn)
        {

            return View(sn);
        }


        [HttpPost]
        public IActionResult Upload(IFormFile myimg, int sn )
        {
            //if (myimg != null && myimg.Length > 0)
            //{
            //    string fileName = $"{sn.ToString()}{Path.GetExtension(myimg.FileName)}";
            //    string basePath = _env.WebRootPath;
            //    string dirPath = Path.Combine(basePath, "Image");

            //    //創建資料夾
            //    if(!Directory.Exists(dirPath))
            //    {
            //        Directory.CreateDirectory(dirPath);
            //    }


            //    using (var stream = new FileStream(Path.Combine(dirPath, fileName), FileMode.Create))
            //    {
            //        myimg.CopyTo(stream);
            //    }
            //}

            //將image轉成jpg檔
            if (myimg != null && myimg.Length > 0)
            {
                
                string fileName = $"{sn.ToString()}.jpg";
                string basePath = _env.WebRootPath;
                string dirPath = Path.Combine(basePath, "Image");

                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }

                
                using (var stream = new MemoryStream())
                {
                    myimg.CopyTo(stream);
                    using (var image = Image.FromStream(stream))
                    {
                        string filePath = Path.Combine(dirPath, fileName);
                        image.Save(filePath, ImageFormat.Jpeg);
                    }
                }
            }
            return RedirectToAction("Privacy", "Home");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Display(int id)
        {
            var imageInfo = imageInfoList.FirstOrDefault(i => i.Id == id);

            if (imageInfo != null)
            {
                string imagePath = Path.Combine("/Images/", imageInfo.Name);
                return View(imagePath);
            }

            return NotFound();
        }

    }
}

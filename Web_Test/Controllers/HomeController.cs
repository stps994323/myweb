﻿using Azure.Messaging;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Linq;
using Web_Test.Models;
using Web_Test.Models.Logic;
using PagedList;
using Azure;

namespace Web_Test.Controllers
{
    public class HomeController : Controller
    {
        int pageSize=5;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index(int page = 1)
        {
            int currentPage = page < 1 ? 1 : page;
            var student_Select = new Student_Select();
            var students = student_Select.GetStudents().OrderByDescending(o => o.UpdateTime).ToList();
            var result = students.ToPagedList(currentPage, pageSize);
            return View(result);

            //var student_Select = new Student_Select();
            //var list = student_Select.GetStudents().OrderByDescending(o => o.UpdateTime).Take(5);
            //return View(list.ToList());
        }
       
        //public IActionResult Privacy(int page = 1)
        //{
        //    int currentPage = page <1 ? 1 : page;
        //    var messagelogic = new Messagelogic();
        //    var messages = messagelogic.GetMessage().OrderByDescending(o => o.UpdateTime).ToList();
        //    var result = messages.ToPagedList(currentPage, pageSize);
            
        //    return View(result);

        //}     

        public IActionResult Privacy(int page = 1, string searchString = "")
        {
            int currentPage = page < 1 ? 1 : page;
            var messagelogic = new Messagelogic();
            var messages = messagelogic.GetMessage() ?? Enumerable.Empty<Models.Entity.MessageBoard>();

            if (!String.IsNullOrEmpty(searchString))
            {
                messages = messages.Where(w => w.Name != null && w.Name.Contains(searchString));
            }

            var orderedMessages = messages.OrderByDescending(o => o.UpdateTime).ToList();
            var result = orderedMessages.ToPagedList(currentPage, pageSize);

            return View(result);
        }

        [HttpGet]
        public IActionResult MessageUpdate(int sqlKey)
        {
            Messagelogic messagelogic = new Messagelogic();

            
            var model = messagelogic.GetMessage().FirstOrDefault(f => f.Sn == sqlKey);
            return View(model);

        }

        [HttpPost]

        public IActionResult SaveStudent(string Id, string Name)
        { 
          Student_Select student_Select = new Student_Select();
            student_Select.SaveStudent(Id, Name);

            return Json("儲存成功");
        }

        [HttpPost]
        public IActionResult DeleteStudent(string Id , string Name) 
        {
          Student_Select student_Select=new Student_Select();
            student_Select.DeleteStudent(Id);

            return Json("刪除啦");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return base.View(new Models.ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]

        public IActionResult SaveMessage(string Name , string Message)
        {
            Messagelogic messagelogic = new Messagelogic();
            messagelogic.SaveMessage(Name, Message);


               return Json("新增成功");
        }

        [HttpPost]
        public IActionResult UpdateMessage(string name, string message , int sn )
        {
            Messagelogic messagelogic = new Messagelogic();
            messagelogic.UpdateMessage(name, message , sn);

            return Json("更新成功");
        }


        [HttpPost]
        public IActionResult DeleteMessage(int Sn)
        {
            Messagelogic messagelogic = new Messagelogic();
            messagelogic.DeleteMessage(Sn);

            return Json("刪除啦");
        }

      
        



    }
}
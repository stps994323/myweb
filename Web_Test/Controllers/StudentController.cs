﻿using Microsoft.AspNetCore.Mvc;
using Web_Test.Models.Logic;
namespace Web_Test.Controllers
{
    public class StudentController : Controller
    {
        [HttpGet]

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult IndexPartial()
        {

            Student_Select student_Select = new Student_Select();

            var model = student_Select.GetStudents().OrderByDescending(o => o.UpdateTime).Take(10).ToList();

            return PartialView("_IndexPartial", model);
        }

        [HttpPost]
        public IActionResult SaveData(string id, string name)
        {

            Student_Select student_Select = new Student_Select();

            student_Select.SaveStudent(id, name);
            return Json("新增成功");

        }

        [HttpPost]

        public IActionResult UpdateData()
        {
            return Json("更新成功");
        }


        [HttpPost]
        public IActionResult DeleteData(string id)
        {

            Student_Select student_Select = new Student_Select();

            student_Select.DeleteStudent(id);

            return Json("刪除成功");

        }
    }
}
